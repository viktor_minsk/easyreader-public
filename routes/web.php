<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/test', function () {
    return view('test');
});



Auth::routes();
Route::group(['middleware'=>'guest'], function () {
   Route::match(['get', 'post'], '/register', 'Auth\RegisterController@checkInvite')->name('register_invite');
   // Route::match(['get', 'post'], '/register2', 'Auth\RegisterController@checkInvite')->middleware('')
   //Route::get('/register')->name('register');


        Route::get('/register2', 'Auth\RegisterController@showRegistrationForm')->middleware('invite')->name('register2');
        Route::post('/register2', 'Auth\RegisterController@register')->middleware('invite')->name('register2');


    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'Auth\LoginController@login')->name('login');

});




Route::get('/contact', function () { return view('register.contact'); })->name('contact');

//Route::get('/login/vk', 'Auth\LoginController@vk')->name('auth/login');
//Route::get('/login/facebook', 'Auth\LoginController@facebook');
//Route::get('/login/social_redirect/vk', 'Auth\LoginController@vkCallback');
//Route::get('/login/social_redirect/facebook', 'Auth\LoginController@facebookCallback');

Route::get('/view/{id}', 'ReaderController@view')->name('view');

Route::group(['middleware'=>'auth'], function ()
{
    Route::get('/home', 'HomeController@ListBooks')->name('home')->middleware('auth');

    Route::get('/add/file', function () { return view('add_file'); })->name('add_file');
    Route::post('/add/file', 'HomeController@addFilePost')->name('add_file');
    Route::get('/add/text', function () { return view('add_text'); })->name('add_text');
    Route::post('/add/text', 'HomeController@addTextPost')->name('add_text');


    Route::post('/delete/{id}', 'HomeController@delete')->name('delete');
    Route::post('/rename/{id}', 'HomeController@rename')->name('rename');

    Route::post('/save_reader_settings', 'HomeController@saveReaderSettings')->name('save_reader_settings');

    Route::get('/register/done', function () { return view('register.done'); })->name('reg_done');

});




Route::resource('books2', 'BooksController');
Route::get('books2/add', 'BooksController@add2');

Route::get('/logout', 'Auth\LoginController@logout');

