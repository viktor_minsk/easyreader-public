<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MultiInviteAndDemoBook extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table)
        {
            $table->string('invite_reason')->nullable();

        });


        Schema::table('invites', function(Blueprint $table)
        {
            $table->dropColumn('used');
            $table->integer('left')->default(1);

        });

        Schema::table('books', function(Blueprint $table)
        {
            $table->boolean('is_demo')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
