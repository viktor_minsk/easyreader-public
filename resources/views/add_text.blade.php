@extends('layouts.main')

@section('head')


    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.js"></script>
@endsection

@section('content')

    <main class="page lanidng-page">
        <section>
            <h1>&nbsp;</h1>
            <div class="container">
            <p>В форму ниже вы можете вставить текст для последующего чтения. Картинки в данный момент не поддерживаются.</p>

            <form action="{{ route('add_text') }}"  method="POST" >
                {{csrf_field()}}
                @if ($errors->any())
                    <div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <span>{{$errors->first('msg')}}</span></div>
                @endif
                <div class="form-group"><label>Заголовок документа:</label><input type="text" name="title" class="form-control" /></div>
                <div class="form-group"><label>Текст:</label><textarea name="content" id="summernote"></textarea></div>
                <div class="form-group"><button class="btn btn-primary" type="submit">Продолжить</button><span></span>
                    &nbsp;&nbsp;<a class="btn btn-light" role="button" href="{{route('home')}}">Отмена</a></div>
            </form>





                <script>
                    $('#summernote').summernote({
                        placeholder: 'Скопируйте и вставьте текст',
                        tabsize: 2,
                        height: 400
                    });
                </script>
            </div>

        </section>
    </main>
@endsection