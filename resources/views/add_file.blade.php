@extends('layouts.main')

@section('content')
    <main class="page lanidng-page">
        <section>
            <h1>&nbsp;</h1>
            <div class="container">
                @if ($errors->any())
                <div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <span>{{$errors->first()}}</span></div>
                @endif
                <h1 class="text-center">Загрузить файл</h1>

                <form method="POST" action="{{ route('add_file') }}" enctype="multipart/form-data">
                    {{csrf_field()}}

                    Файл: <input type="file" name="file" />
                    <br/>
                    <br/>
                    <input type="submit"  class="btn btn-primary"  value="Загрузить"/>&nbsp;&nbsp;<a class="btn btn-light" href="{{route('home')}}">Отмена</a>
                    <br/><br/>
                    В данный момент поддерживаются форматы:
                    <li><b>epub</b> - лучше и быстрее всего загружается</li>
                    <li>fb2</li>
                    <li>docx (именно docx, а не doc)</li>
                    <li>Будьте внимательны. Обработка больших docx или fb2 может занять много времени.</li>
                    <br/><br/>



        </section>
    </main>
@endsection