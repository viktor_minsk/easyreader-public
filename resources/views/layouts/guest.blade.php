<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Easy Reader</title>
    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Work+Sans">
    <link rel="stylesheet" href="/assets/css/Navigation-Clean.css">
    <link rel="stylesheet" href="/assets/css/style.css?{{time()}}">
    <link rel="stylesheet" href="/assets/css/untitled-1.css">
    <link rel="stylesheet" href="/assets/css/untitled.css">
</head>


<body>
    <nav class="navbar navbar-light navbar-expand-lg fixed-top bg-white clean-navbar" style="background-color:rgb(210,2,2);">
        <div class="container"><a class="navbar-brand logo" href="/">Easy Reader</a><button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse"
                id="navcol-1">
                <ul class="nav navbar-nav ml-auto">
                    <li class="nav-item" role="presentation"><a class="nav-link" href="/">Главная</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="{{route('contact')}}">Контакты</a></li>
					@guest
                    <li class="nav-item" role="presentation"><a class="nav-link" href="{{route('register_invite')}}">Регистрация</a></li>		
                    <li class="nav-item" role="presentation"><a class="nav-link" href="{{route('login')}}">Вход</a></li>
					@else
					    <li class="nav-item" role="presentation"><a class="nav-link" href="{{route('login')}}">Личный кабинет</a></li>
				 <li class="nav-item" role="presentation"><a class="nav-link" href="{{route('login')}}">Выход</a></li>										
					@endguest
                </ul>
            </div>
        </div>
    </nav>
 	@yield('content')
    <footer class="page-footer dark">
        <div class="footer-copyright">
            <p>© 2019</p>
        </div>
    </footer>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.0/js/bootstrap.bundle.min.js"></script>
    <script src="/assets/js/theme.js"></script>
</body>

</html>

