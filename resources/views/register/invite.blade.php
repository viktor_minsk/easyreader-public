@extends('layouts.guest')

@section('content')
<main class="page registration-page">
    <section class="clean-block clean-form dark">
        <div class="container">
            <div class="block-heading">
                <h2 class="text-info">Регистрация</h2>
                <p>В данный момент идет тестирование сервиса. Регистрация только по приглашениям. Для получения приглашения,
                    <a style="color: rgb(0, 123, 255);"  href="{{route('contact')}}">напишите мне</a>.</p>
            </div>
            <form method ="POST" action="{{route('register_invite')}}">
				{{ csrf_field() }}
                <div class="form-group"><label for="name">Приглашение:</label><input style="text-transform: uppercase;" class="form-control item" name="code" type="text" id="name"><span class="text-danger">{{ $errors->first('code') }}</span></div><button class="btn btn-dark btn-block" type="submit">Продолжить</button>
			</form>
        </div>
    </section>
</main>

@endsection