@extends('layouts.guest')

@section('content')
<main class="page registration-page">
<section class="clean-block clean-form dark">
    <div class="container">
        <div class="block-heading">
            <h2 class="text-info">Контакты</h2>
            <p></p>
        </div>
        <form>
            <div class="form-group"><label >
Вы можете связаться со мной по любым вопросам, используя данные контакты:<br/><br/>
				<i class="fab fa-telegram"></i> Telegram: <a target="_blank" style="color: black !important;" href="https://t.me/viktor_r1">@viktor_r1</a><br/><br/>
				<i class="fab fa-vk"></i> VK: <a style="color: black !important;" target="_blank" href="https://vk.com/id3815312">https://vk.com/id3815312</a><br/><br/>
								<i class="fab fa-facebook"></i> VK: <a style="color: black !important;" target="_blank" href="https://www.facebook.com/profile.php?id=100002146088425">https://www.facebook.com/profile.php?id=100002146088425</a><br/><br/>
								
								<i class="far fa-envelope"></i> e-mail: 3733841@gmail.com				
                </label>
            </div>
			
        </form>
    </div>
</section>
</main>




@endsection