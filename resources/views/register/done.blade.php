@extends('layouts.guest')

@section('content')
<main class="page registration-page">
<section class="clean-block clean-form dark">
    <div class="container">
        <div class="block-heading">
            <h2 class="text-info">Регистрация</h2>
            <p></p>
        </div>
        <form>
            <div class="form-group"><label>Вы успешно зарегистрировались! В дальнейшем, для входа используйте эти данные:<br /><br />e-mail:&nbsp;&nbsp;<strong>{{ Auth::user()->email }}</strong><br /><br />Пароль: <strong>&lt;выбранный вами пароль&gt;</strong></label></div>
				<a class="btn btn-dark btn-block" role="button" href="{{route('home')}}">ПРОДОЛЖИТЬ</a>
        </form>
    </div>
</section>
</main>




@endsection