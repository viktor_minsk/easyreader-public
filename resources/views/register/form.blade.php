@extends('layouts.guest')

@section('content')
<main class="page registration-page">
    <section class="clean-block clean-form dark">
        <div class="container">
            <div class="block-heading">
                <h2 class="text-info">Регистрация</h2>
                <p>Заполните поля регистрационной формы:</p>
            </div>
            <form>
                <div class="form-group"><label for="email">e-mail:</label><input type="text" class="form-control item" value="{{ old('email') }}" name="email" id="email" /><span class="text-danger">{{ $errors->first('name') }}</span></div>
				
                <div class="form-group"><label for="name">Как вас зовут?</label><input type="text"  value="{{ old('name') }}" class="form-control item" name="name" id="name" /><span class="text-danger">{{ $errors->first('name') }}</span></div>
                <div class="form-group"><label for="password">Пароль</label><input type="password" class="form-control item" name="password" id="password" /></div>
                <div class="form-group"><label for="password">Подтверждение пароля:</label><input type="password" class="form-control item" name="password_confirmation" id="confirm" /></div>

                <div class="form-group " ><label for="name">Почему вас заинтересовал сервис?</label><textarea  name="why" class="form-control" style="min-height:160px;;"> value="{{ old('why') }}"</textarea></div><button class="btn btn-dark btn-block"  type="submit">Зарегистрироваться</button></form>
        </div>
    </section>
</main>
</main>

@endsection