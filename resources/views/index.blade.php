@extends('layouts.main')

@section('content')
    <style>
        .table-mod
        {
            display: none;
        }
    </style>

    <script>
        function editOrDelete()
        {
            if($(".table-mod").length==0) return;
            if($(".table-mod").first().is(':visible')) $(".table-mod").hide();
            else $(".table-mod").show();
        }

        function deleteBook(id)
        {
            if(!confirm('Документ будет удален. Отменить это действие невозможно. Продолжить?')) return false;
            url='{{route('delete', ':slug' )}}';
            url=url.replace(':slug', id);
            $.post(url, function ()
            {
                $("#book_row_"+id).fadeOut('slow');
            }).fail(function() {
                    alert('Ошибка при выполнении операции!');
                }
            )
        }


        function renameBook(id)
        {

            currentName=$('#book_name_'+id).html();

            newName=prompt('Введите новое имя для документа:', currentName)
            if(!newName) return false;


            url='{{route('rename', ':slug' )}}';
            url=url.replace(':slug', id);


            $.post(url, {'name': newName}, function (res)
            {
                    $("#book_name_"+id).fadeOut('slow', function()
                    {
                        $('#book_name_'+id).html(res);
                        $('#book_name_'+id).fadeIn("slow");
                    })
            }).fail(function() {
                    alert('Ошибка при выполнении операции!');
                }
            )
        }



        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>

  <main class="page landing-page">
      <section class="clean-block clean-info dark">
          <h1>&nbsp;</h1>
          <div class="container">


                  <div class="block-heading" style="font-size:18px;;">
                  <h1 class="">Мои документы</h1>
                      @if (session('status'))
                      <div role="alert" class="alert alert-success">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                          <span>{{ session('status') }}</span>
                      </div>
                      @endif

                  </div>



                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Имя</th>
                                <th>Чтение</th>
                                <th colspan="2" class="table-mod">Действия</th>

                            </tr>
                        </thead>
                        <tbody>
                        @foreach($books as $book)
                            <tr id="book_row_{{$book->id}}">
                                <td >{{$book->id}}</td>
                                <td><span  id="book_name_{{$book->id}}">{{$book->name}}</span></td>
                                <td ><a href="{{route('view', $book->id)}}"  class="btn btn-dark" role="button" href="#">Читать</a>&nbsp;</td>
                                <td class="table-mod"><a href="#" onclick="renameBook({{$book->id}})" class="btn btn-warning" >Переименовать</a></td>
                                <td class="table-mod"><a href="#" onclick="deleteBook({{$book->id}}); return false;" class="btn btn-danger" >Удалить</a></td>


                            </tr>

                         @endforeach
                        </tbody>
                    </table>
                 </div>
          </div>
      </section>
      <section class="clean-block clean-info dark ribbon ribbon-content" style="">


          <div class="container">
              <div>Загрузите файл или добавьте текст для последующего чтения:<br/><br/></div>
              <a class="btn btn-link" role="button" href="{{route('add_file')}}" style="background-color:rgb(54, 54, 57);color: white;">Загрузить Файл</a>&nbsp;&nbsp;
              <a class="btn btn-link" role="button"  href="{{route('add_text')}}" style="background-color:rgb(54, 54, 57);color: white;">Добавить текст</a><br/><br/>
                <a href="#" onclick="editOrDelete(); return false;">Удалить / Переименовать</a>

          </div>
      </section>
  </main>
@endsection