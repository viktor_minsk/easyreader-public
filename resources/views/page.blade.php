<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{$title}} - Easy Reader</title>
    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Work+Sans">
    <link rel="stylesheet" href="/assets/css/Navigation-Clean.css">
    <link rel="stylesheet" href="/assets/css/reader.css">
    <link rel="stylesheet" href="/assets/css/style.css">
    <link rel="stylesheet" href="/assets/css/untitled.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
    <script src="/assets/js/lazy.js?{{time()}}"></script>

    <style>
        .reader-area {
            /*background: #f0e8d1;*/
            padding-top:20px;
            padding-bottom:20px;
            max-width:600px;
            word-wrap:break-word;
            font-family: 'Lora','Georgia','Times New Roman',serif;
            font-weight: 400;
            font-style: normal;
            font-size: 18px;
            line-height: 1.8;
        }



    </style>

</head>

<body>



<body style="background-color:#f1e9d2;;">
<div>
    <nav class="navbar navbar-light navbar-expand navigation-clean" style="background-color:black;">
        <div class="container-fluid"><a  class="navbar-brand r_ignore" href="{{$back}}">{{$title}}</a><button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse"
                 id="navcol-1">
                <ul class="nav navbar-nav ml-auto">
                    @auth
                    <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="modal" data-target="#modal"  href="#"><i style="font-size: 32px; " class="fas fa-cog"></i></a></li>
                        @endauth;
                </ul>
            </div>
        </div>
    </nav>
</div>
<div class="container reader-area">
    <article>
        <a href="{{$back}}" style="text-decoration: underline;">← вернуться к содержанию</a>
        <p style="word-wrap: break-word;">{!! $content !!}</p>
    </article>
</div>




<div class="modal fade" class="r_ignore" id="modal" role="dialog" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" action="{{route('save_reader_settings')}}" class="r_ignore">
            <div class="modal-header">
                <h4 class="modal-title ">Настройки чтения</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>

            <div class="modal-body">

                    {{csrf_field()}}
                @auth
                    <div class="form-group"><label>Режим чтения:</label><select name="mode" class="form-control">
                            <option     @if  (Auth::user()->reader_options['mode']=='lazy') selected="selected" @endif; value="lazy" >Easy Reader</option>
                            <option value="simple" @if (Auth::user()->reader_options['mode']=='simple') selected="selected" @endif; >Обычный</option>
                            </select></div>
                @endauth
                   {{--  <div class="form-group"><label>Шрифт:</label><select name="font" class="form-control"><option value="simple">Обычный</option><option value="monospace">Monospace</option></select></div>--}}

            </div>
            <div class="modal-footer"><button class="btn btn-dark r_ignore" type="submit">Сохранить</button></div>
            </form>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.0/js/bootstrap.bundle.min.js"></script>
<script src="/assets/js/theme.js"></script>


</body>


<script>
    @auth
        @if (Auth::user()->reader_options['mode']!='simple')
            colorthepage();
        @endif;
    @else
        colorthepage();
    @endauth;
</script>
</body>
</html>
