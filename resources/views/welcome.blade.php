@extends('layouts.guest')

@section('content')
    <main class="page landing-page">
        <section class="clean-block clean-hero" style="color:rgba(64,69,71,0.85);background-image:url(&quot;assets/img/nature-3048299_960_720.jpg&quot;);">
            <div class="text">
                <h2>EASY READER</h2>
                <p>Хочется читать больше? Не успеваете?<br>Я знаю, каково это...<br>Данный сервис поможет вам!</p><a class="btn btn-outline-light btn-lg" role="button" href="#more1">Подробнее</a></div>
        </section>
        <section id="more1" class="clean-block clean-info dark">
            <div class="container">
                <div class="block-heading">
                    <h2 class="text-info" id="more1" style="color:rgb(238, 238, 238);font-size:36px !important;;">ДОБРО ПОЖАЛОВАТЬ!</h2>
                    <p style="max-width:700px;;">Однажды, я увлекся психологией, и стал читать много книг по этой теме. Информация в них была ценна, но местами они были скучноваты. В процессе чтения, мог забыть какую строчку я читаю сейчас или перечитывать одно и то же несколько
                        раз...&nbsp;<br>Захотелось решить эту проблему и я создал Easy Reader!<br></p>
                </div>
                <div class="row">
                    <div class="col text-center"><img src="/assets/img/logo.jpg" /></div>
                </div>
                <div class="row">
                    <div class="col">
                        <p class="text-right"><strong>Обычное чтение</strong></p>
                    </div>
                    <div class="col">
                        <p class="text-left"><strong>Используя Easy Reader</strong></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <p>Известно, что у человека визуальное восприятие развито лучше всего. Яркие цвета всегда привлекают наше внимание. Именно это свойство было использовано при разработке сервиса. Таким образом удается решить проблемы монотонности и
                            "перескакивания" со строчки на строчку.&nbsp;<a target="_blank" style="color: rgb(0, 123, 255);" href="{{route('view',16)}}">Демонстрация работы сервиса</a>.<br></p>
                    </div>
                </div>
            </div>
        </section>
        <section class="clean-block features">
            <div class="container">
                <div class="block-heading">
                    <h2 class="text-info">Возможности</h2>
                    <p style="max-width:600px;;">&nbsp;</p>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-5 feature-box"><i class="icon-star icon"></i>
                        <h4>Поддержка основных форматов</h4>
                        <p>EPUB, FB2, DOCX. Список форматов будет расширяться.</p>
                    </div>
                    <div class="col-md-5 feature-box"><i class="icon-pencil icon"></i>
                        <h4>Способствует продуктивности</h4>
                        <p>Повышает скорость чтения без потери качества.</p>
                    </div>
                    <div class="col-md-5 feature-box"><i class="icon-screen-smartphone icon"></i>
                        <h4>Совместимость с любыми устройствами.</h4>
                        <p>Телефон, компьютер или планшет? Адаптировано для любых устройств. Не требует установки приложений!</p>
                    </div>
                    <div class="col-md-5 feature-box"><i class="icon-refresh icon"></i>
                        <h4>Облачное хранение книг и документов</h4>
                        <p>Где бы вы ни находились, ваши документы всегда с собой!</p>
                    </div>
                </div>
            </div>
        </section>
        <section class="clean-block slider dark">
            <div class="container">
                <div class="block-heading">


                    <h2 class="text-info">Попробуем?</h2>
                    <p style="max-width:600px;;">На данный момент сервис делается для души и является полностью бесплатным.&nbsp;Для продолжения, войдите либо зарегистрируйтесь.<br>&nbsp;</p>
                    <div class="row" style="padding-top:10px;">
                        <div class="col"><a class="btn btn-dark" role="button" href="{{route('login')}}" style="margin-left:20px;;">Вход</a><a class="btn btn-dark" role="button" href="{{route('register_invite')}}" style="margin-left:20px;;">Регистрация</a></div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection