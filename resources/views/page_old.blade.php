<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <script src="/js/colorit.js"></script>
        <!-- Styles -->
        <style>
			body {
			  background: #f1e9d2;
			  color: #333;
			  font-family: 'Lora','Georgia','Times New Roman',serif;
			  font-weight: 400;
			  font-style: normal;
			  font-size: 18px;
			  line-height: 1.6;
			  padding: 0;
			  margin: 0;
			}

            article {
                /*border: 1px solid;*/
                background: #f0e8d1;
                width: 600px;
                max-width: 100%;
                margin: 0 auto;
                padding: 20px 20px;
            }

			figure {
			  margin: 30px 0;
			}

			img {
			  max-width: 100%;
			}
        </style>
    </head>
    <body>
<article>
	  {!! $content !!}
</article>
	  <script>
		  colorthepage();
	  </script>
    </body>
</html>
