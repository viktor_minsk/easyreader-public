@extends('layouts.main')

@section('content')
  <main class="page landing-page">
      <section class="clean-block clean-info dark" >
          <div class="container" style="padding-top: 30px; text-align: left !important;;">
              <a href="{{route('home')}}" >← к списку документов</a><br/><br/>
<h1>{{ $book->name }} </h1>

@foreach($toc as $t)


@for($i=0; $i<$t['offset']; $i++)
    &nbsp;&nbsp;&nbsp;
@endfor

• <a class="toc" href="{{ route('view', ['id'=>$book->id, 'src'=>$t['src'], $t['hash'] ] ) }}">{{ $t['text'] }} </a><br/>

@endforeach



</div>
</section>
</main>
@endsection