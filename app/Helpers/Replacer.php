<?php
namespace App\Helpers;
use mysql_xdevapi\Exception;/**
 * User: viktor
 * Date: 2019-01-20
 * Time: 22:37
 */

class Replacer {
    static public function replace($book, $content, $base)
    {
        try {
            $contentPath=$book->getContentPath();
            $path = Path::join($contentPath, dirname($base));
            libxml_use_internal_errors(false);
            $doc = new \DOMDocument();


            @$doc->loadHTML($content);

            $tags = $doc->getElementsByTagName('img');


            foreach ($tags as $tag) {
                $old_src = $tag->getAttribute( 'src');
                $v=realpath($path.'/'.$old_src);
                if(Path::validPath($contentPath, $v)) $new_src=Path::getURI($contentPath.'/', $v);
                else $new_src='';
                $new_src_url=route('view', ['id'=>$book->id, 'src'=>$new_src]);
                $tag->setAttribute('src', $new_src_url);
            }



            $tags = $doc->getElementsByTagName('a');

            foreach ($tags as $tag) {

                $old_src = $tag->getAttribute( 'href');
                $h=explode('#', $old_src);
                if(isset($h[1])) $h1='#'.$h[1];
                else $h1='';


                $v=realpath($path.'/'.$h[0]);

                if(Path::validPath($contentPath, $v)) $new_src=Path::getURI($contentPath.'/', $v);
                else $new_src='';

                $new_src_url=route('view', ['id'=>$book->id, 'src'=>$new_src, $h1]);
                $tag->setAttribute('href', $new_src_url);
            }


            return @$doc->saveHTML();
        } catch (\Exception $e)
        {

            return $content;
        }
    }
}