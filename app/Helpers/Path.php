<?php
namespace App\Helpers;
/**

 * User: viktor
 * Date: 2019-01-20
 * Time: 22:37
 */

    class Path {
        static public function join($base, $target)
        {
            $path=realpath($base.'/'.$target);
            return self::validPath($base, $path);
        }

        static public function validPath($base, $path)
        {
            if(!$path || substr($path, 0, strlen($base))!=$base ) return false;
            if(is_file($path) || is_dir($path)) return $path;
        }

        static public function getURI($base, $path)
        {
            return substr_replace($path, '',0,strlen($base));
        }

        static function basename($path)
        {
            $a=explode( '.', basename($path));
            return $a[0];
        }
    }