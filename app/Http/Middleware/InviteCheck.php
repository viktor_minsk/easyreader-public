<?php

namespace App\Http\Middleware;

use App\Models\Invite;
use Closure;


class InviteCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $code = $request->session()->get('invite', '');
        if(!Invite::checkInvite($code)) return redirect()->route('register_invite');
        return $next($request);
    }
}
