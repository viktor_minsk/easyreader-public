<?php

namespace App\Http\Controllers\Auth;

use App\Models\Book;
use App\Models\Invite;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;




    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/register/done';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    public function checkInvite(Request $request)
    {


        if(session('invite', '') && Invite::checkInvite(session('invite'))) return redirect()->route('register2');



        if( $request->isMethod('post')  )
        {
            if(Invite::checkInvite($request->input('code'))) {
                session(['invite'=>$request->input('code')]);
                return redirect()->route('register2');
            }
            else {
                return back()->withErrors(['code'=>'Введен неверный код или приглашение уже использовано']);

            }


        }




        return view('register.invite');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'why' => ['required', 'string', 'min:10'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        if($invite= Invite::checkInvite(session('invite'))) {
            $invite->left--;
            $invite->save();
        }


        $user= User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'why'=>$data['why'],
            'invite_reason'=>$invite->comment,
        ]);



        foreach(config('app2.seed_books') as $book)
        {
            $book['user_id']=$user->id;
            Book::create($book);
        }

        return $user;


    }
}
