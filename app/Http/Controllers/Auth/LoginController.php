<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
//use http\Env\Request;
use Facebook\Facebook;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use Illuminate\Http\Request;
use App\User;
use  ATehnix\VkClient\Client as VkClient;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function vk(Request $request)
    {
        return Socialite::with('vkontakte')->redirect();
    }

    public function facebook(Request $request)
    {
        return Socialite::with('facebook')->scopes(['user_friends'])->redirect();
    }


    public function vkCallback(Request $request)
    {
        $social = Socialite::driver('vkontakte')->user();

        $user=User::findOrCreateSocial('vk', $social);
        $vk=new VkClient();
        $r=$vk->request('friends.get', [], $social->token);
        var_dump($r);
        print_r($r); die;
        \Auth::login($user, true);
        return redirect('home');

    }


    public function facebookCallback(Request $request)
    {
        $social = Socialite::driver('facebook')->user();

        $user=User::findOrCreateSocial('facebook', $social);
        //print_r($social); die;

        $config = config('services.facebook');
        $fb=new Facebook([
            'app_id' => $config['client_id'],
            'app_secret' => $config['client_secret'],
            'default_graph_version' => 'v2.6',
        ]);

        $fb->setDefaultAccessToken($social->token);
        print_r($fb->get('/me/friends', $social->token));
        die;
        //100002146088425
        var_dump($r);
        print_r($r); die;
        \Auth::login($user, true);
        return redirect('home');

    }
}
