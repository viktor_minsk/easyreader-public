<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\BookRequest as StoreRequest;
use App\Http\Requests\BookRequest as UpdateRequest;

/**
 * Class BookCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class BookCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Book');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/book');
        $this->crud->setEntityNameStrings('book', 'books');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
       // $this->crud->setFromDb();


       $this->crud->addColumn([   'name' => 'id','label' => 'ID', 'type' => 'text',]);
       $this->crud->addColumn([   'name' => 'name','label' => 'Name', 'type' => 'text',]);
       $this->crud->addColumn([   'name' => 'author','label' => 'Author', 'type' => 'text',]);
       $this->crud->addColumn([   'name' => 'file','label' => 'File', 'type' => 'text',]);       


       //$this->crud->addButton('top', '', , 'end');
       $this->crud->addButtonFromView('top', 'text', 'add_text', 'end');
       $this->crud->addButtonFromView('line', 'read', 'read', 'end');       
       //$this->crud->addField([   'name' => 'name', 'label' => 'Name','type' => 'text',]);
       //$this->crud->addField([   'name' => 'author', 'label' => 'Author','type' => 'text',]);
        $mode=request('mode', 'file');
        if($mode=='file')
        {
            $this->crud->addField([   // Upload
                'name' => 'file',
                'label' => 'Book',
                'type' => 'upload',
                'upload' => true,
                'disk' => 'uploads' // if you store files in the /public folder, please ommit this; if you store them in /storage or S3, please specify it;
            ],'create');

        
        
        
            $this->crud->addField([   // Upload
                'name' => 'file',
                'label' => 'File',
                'type' => 'text',
                'attributes'=>['readonly'=>'readonly'],
           ],'update');
           //$this->crud->removeButton( 'update' );
        }
        else {
            $this->crud->addField([   
                'name' => 'name',
                'label' => 'Name',
                'type' => 'text',   
            ]);

            $this->crud->addField([   
                'name' => 'author',
                'label' => 'Author',
                'type' => 'text',   
            ]);

            $this->crud->addField([   
                'name' => 'content',
                'label' => 'Content',
                'type' => 'tinymce',   
                'attributes'=>[
                    'style'=>'height: 400px'
                ]
            ]);
        
        }

        $this->crud->addField([   
            'name' => 'mode',
            'value'=>$mode,
            'type' => 'hidden',   
        ]);

        
     
        // add asterisk for fields that are required in BookRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
           
        $mode=request('mode', 'file');
        $redirect_location = parent::storeCrud($request);

        $book=$this->crud->entry;
        if($mode=='file') {
            $book->extract();
            if(!$book->extractTest())
            {
                $book->delete();
                return \Redirect::back()->withErrors(['file'=> 'File has not passed test']);
           }

        $book->applyInfo();
        }
        $book->save();

       
        return $redirect_location;
    }



    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $mode=request('mode', 'file');

        if($mode=='file') return \Redirect::back()->withErrors(['error'=> 'No editting allowed']);
        $redirect_location = parent::updateCrud($request);

        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry

        return $redirect_location;
    }
}
