<?php

namespace App\Http\Controllers;
use App\Helpers\Path;
use App\Helpers\Replacer;
use App\Models;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Html2Text\Html2Text;
use Facades\App\Classes\XMLHelper;

class ReaderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->authorizeResource(Models\Book::class);

    }




    public function view(Request $request, $id)
    {

        $book=Models\Book::loadBook($id);
        if(!$book->is_demo) $this->authorize('view',$book);

        if($book->type=='inline')  return $this->inline($book);

        $src=$request->input('src');
        if(!$src && $book->isSingleFile()) $src=$book->isSingleFile();
        if($src) return $this->part($book, $src);

        $info=$book->extractInfo();


        $xml=new \DOMDocument();
        $xml->load($info['ncx']);
        $items=$xml->getElementsByTagName('navPoint');
        $toc=[];
        $initialParentsCount=0;
        foreach($items as $i)
        {
            if(!$initialParentsCount) $initialParentsCount=XMLHelper::parentsCount($i);

            
            $simple=simplexml_import_dom($i);

            $src=(string) $simple->content['src'];
            $a=explode('#', $src);
            if(!isset($a[1])) $hash='';
            else $hash='#'.$a[1];

            $offset=XMLHelper::parentsCount($i)-$initialParentsCount;
            if($offset<0) $offset=0;
            $toc[]=['text'=>(string) $simple->navLabel->text, 'src'=>$a[0], 'hash'=>$hash, 'offset'=>$offset];
        }

        return view('toc', ['book'=>$book, 'toc'=>$toc]);
    }

    public function inline(Models\Book $book)
    {
        if(Auth::user()) $back=route('home');
        else $back='/';

        return view('page', ['content' => clean($book->content), 'title'=>$book->name, 'back'=>$back]);
    }




    public function part(Models\Book $book, $src)
    {
        $path=$book->getContentPath();

        try{
            $target=Path::join($path,$src);

            if(!is_file($target)) abort(404, 'File not found!');

            $mime=mime_content_type($target);

            if( (strstr($mime,'text/') && strstr($mime,'html') ) || strstr($mime, 'xml') )  {

                $f = @file_get_contents(Path::join($path, $src));

                $f = Replacer::replace($book, $f, $src);
                if (!$f) abort('404', 'Part not found');
                preg_match("/<body[^>]*>(.*?)<\/body>/is", $f, $match);

                if(isset($match[1])) $content = clean($match[1]);
                else $content=clean($f);

                $back=route('view', $book->id);
                if($book->isSingleFile()) $back=route('home');
                return view('page', ['content' => $content, 'title'=>$book->name, 'back'=>$back]);
            }
            elseif (strstr($mime, 'image/'))
            {
                    header('Content-Disposition: inline');
                    header('Content-type: '.$mime);
                    readfile($target);
                    return;
            }

        }catch(Exception $e)
        {
            abort(404);
        }



    }


}
