<?php

namespace App\Http\Controllers;
use App\Models;
use function GuzzleHttp\default_ca_bundle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Html2Text\Html2Text;
use Mews\Purifier\Purifier;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    //    $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */



    public function ListBooks(Request $request)
    {
        $books= Models\Book::where('user_id',Auth::user()->id)->get();
        return view('index', ['books'=> $books] );
    }



    public function delete(Request $request, $id)
    {

        $book=Models\Book::findOrFail($id);
        $this->authorize('delete',$book);
        $book->delete();
        return 'ok';
    }

    public function rename(Request $request, $id)
    {
        $book=Models\Book::findOrFail($id);
        $this->authorize('rename',$book);
        $name=trim($request->input('name'));
        $book->name=$name;
        $book->save();

        return htmlspecialchars($name, ENT_NOQUOTES);
    }

    function saveReaderSettings(Request $request)
    {
        $user = Auth::user();


        $opts=$user->reader_options;

        $mode=$request->input('mode','');
        if(in_array($mode, ['lazy', 'simple']))   $opts['mode']=$mode;



        $user->reader_options=$opts;
        $user->save();
        return back();
    }

    function addFilePost(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'file' =>  'required'//|mimetypes:application/epub+zip'
        ]);

        if($validator->fails())
        {
            return redirect()->route('add_file')
                ->withErrors($validator);
        }

        $extension=$request->file('file')->getClientOriginalExtension();


        switch ($extension) {
            case 'epub':
            case 'fb2':
            case 'docx':
                $book=new Models\EpubBook();
                break;

            case 'doc':
                return redirect()->route('add_file') ->withErrors(['msg'=>'Пока поддерживается только формат DOCX (не doc!!)']);

            default:
                return redirect()->route('add_file') ->withErrors(['msg'=>'Неверный формат файла']);

        }

        $book->file=$request->file;


        if(!$book->finishAndValidateUpload($extension))
        {
            return redirect()->route('add_file') ->withErrors(['msg'=>'Ошибка обработки файла']);
        }

        $book->user_id=Auth::user()->id;
        $book->save();

        return redirect()->route('home')->with('status', 'Документ успешно добавлен!');

    }


    function addTextPost(Request $request)
    {
        $title=$request->post('title','');
        $content=$request->post('content', '---');

        $book=new Models\Book();
        if(!$title) $title='Без названия';
        $book->name=$title;


        $book->content=clean($content);
        $book->type='inline';
        $book->user_id=Auth::user()->id;

        $book->save();
        return redirect()->route('home')->with('status', 'Докумен успешно добавлен!');

    }




}
