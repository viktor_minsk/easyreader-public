<?php 
namespace App\Classes;

class XMLHelper {
    function parentsCount(\DOMNode $d)
    {
        $count=0;
        while($d->parentNode) {
            $count++;
            $d=$d->parentNode;
        }
        return $count;
    }
}
