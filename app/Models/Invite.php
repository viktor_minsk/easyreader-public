<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invite extends Model
{
    static function checkInvite($code)
    {
        return self::where('code', 'like', (string)$code)
            ->where('left', '>',0)
            ->first();
    }
}
