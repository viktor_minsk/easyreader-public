<?php

namespace App\Models;
use \App\Helpers\Path;
class EpubBook extends Book
{
    //protected $hidden = ['extractPath'];


    public static  function boot()
    {
        parent::boot();

        static::deleting(function($obj) {
            $obj->extractDelete();
            foreach(config('app2.seed_books') as $book)
            {
               if($book->file==$obj->file) return;
            }
            \Storage::disk('public')->delete($obj->file);
        });

    }

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->type='epub';
    }


    public function isSingleFile()
    {
        $path=$this->getExtractionPath();
        if(is_file($path.'/__X_converted_from_docx.html')) return '__X_converted_from_docx.html';
        return false;
    }

    public function extract()
    {

        $extr=$this->getExtractionPath();
        $path=\Storage::disk('public')->path($this->file);
        \Storage::disk('public')->deleteDirectory($extr);
        \Storage::disk('public')->makeDirectory($extr);


        $zip=new \ZipArchive;
        try {

            $zip->open($path);
            $zip->extractTo($extr);
        }catch (\Exception $e)
        {
            return false;
        }

        return $extr;
    }


    public function getExtractionPath()
    {
        $path=\Storage::disk('public')->path($this->file);
        $extr=\Storage::disk('public')->path('uploads/unzip').'/'.Path::basename($this->file);

        return $extr;
    }


    public function getContentPath()
    {
        if($this->isSingleFile())
        {
            return $this->getExtractionPath();
        }
        $all=$this->extractInfo();
        return $all['contentPath'];
        //return $content;
    }


    public function extractDelete()
    {

        $path=\Storage::disk('public')->path($this->file);
        \Storage::disk('public')->deleteDirectory('uploads/unzip/'.basename($path,'.epub'));
    }


    public function extractInfo()
    {

        try {

            $extr = $this->getExtractionPath();
            if (!$extr) return false;
            if (!is_file($extr . '/META-INF/container.xml')) return false;

            $doc = new \DOMDocument();
            $doc->load($extr . '/META-INF/container.xml');
            $opf = $doc->getElementsByTagNameNS('*', 'rootfile')->item(0)->getAttribute('full-path');

            $contentPath=Path::join($extr, '/'.dirname($opf));

            $opf=Path::join($extr, $opf);

            if (!$opf) return false;


            $doc = new \DOMDocument();
            $doc->load($opf);

            $title = $doc->getElementsByTagNameNS('*', 'title')->item(0)->nodeValue;
            $author = $doc->getElementsByTagNameNS('*', 'creator')->item(0)->nodeValue;

            $ncx='';
            foreach($doc->getElementsByTagNameNS('*', 'item') as $ii)
            {
                if($ii->getAttribute('id')=='ncx') $ncx=$ii->getAttribute('href');
            }




            if ($ncx) {
                if($author || $title) $name=$author.' - '.$title;
                else $name='';
                if(!$name) $name=$this->upload_name;

                $ncx=Path::join($contentPath, '/'.$ncx);
                return ['name'=>$name, 'ncx'=>$ncx, 'contentPath'=>$contentPath];

            }
            else return false;
        } catch (\Exception $e)
        {

            return false;
        }
    }



    public function convertFromFb2()
    {
        $app=escapeshellcmd(config('converters.calibre'));
        $path=escapeshellarg(\Storage::disk('public')->path($this->file));
        $target=escapeshellarg(\Storage::disk('public')->path('/uploads/'.Path::basename($this->file).'.epub'));

        $command="$app $path $target";
        shell_exec($command);


        $this->attributes['file']='uploads/'.Path::basename($this->file).'.epub';
    }


    public function convertFromDocx()
    {

        $extr=$this->getExtractionPath();
        $app=escapeshellcmd(config('converters.pandoc'));
        $path=escapeshellarg(\Storage::disk('public')->path($this->file));
        \Storage::disk('public')->makeDirectory($extr);
        \Storage::disk('public')->makeDirectory($extr);
        \Storage::disk('public')->makeDirectory($extr.'/media');
        mkdir($extr);
        $current=getcwd();
        chdir($extr);

        $target=escapeshellarg('__X_converted_from_docx.html');
        $media=escapeshellarg('media');


        $command="$app $path -o $target -s --extract-media=".$media;
        $this->name=$this->upload_name;
        shell_exec($command);

        chdir($current);
        return true;
    }

    public  function finishAndValidateUpload($extension='epub')
    {
        if($extension=='fb2') $this->convertFromFb2();

        if($extension=='docx') return $this->convertFromDocx();


        if(!$this->extract() || !$this->extractTest()) return false;
        $this->applyInfo();
        return true;
    }

    public function uploadFileToDisk($value, $attribute_name, $disk, $destination_path)
    {
        $request = \Request::instance();

        // if a new file is uploaded, delete the file from the disk
        if ($request->hasFile($attribute_name) &&
            $this->{$attribute_name} &&
            $this->{$attribute_name} != null) {
            \Storage::disk($disk)->delete($this->{$attribute_name});
            $this->attributes[$attribute_name] = null;
        }

        // if the file input is empty, delete the file from the disk
        if (is_null($value) && $this->{$attribute_name} != null) {
            \Storage::disk($disk)->delete($this->{$attribute_name});
            $this->attributes[$attribute_name] = null;
        }

        // if a new file is uploaded, store it on disk and its filename in the database
        if ($request->hasFile($attribute_name) && $request->file($attribute_name)->isValid()) {
            // 1. Generate a new file name
            $file = $request->file($attribute_name);
            $new_file_name = md5($file->getClientOriginalName().time()).'.'.$file->getClientOriginalExtension();

            // 2. Move the new file to the correct path
            $file_path = $file->storeAs($destination_path, $new_file_name, $disk);

            // 3. Save the complete path to the database
            $this->attributes[$attribute_name] = $file_path;
        }
    }


    public function extractTest($path=null)
    {
        if(!$this->extractInfo()) return false;
        return true;
    }

    function applyInfo()
    {
        $info=$this->extractInfo();
        $this->name=$info['name'];

    }


    public function setFileAttribute($value)
    {
        $attribute_name = "file";
        $disk = "public";
        $destination_path = "uploads";
        $this->upload_name=pathinfo($value->getClientOriginalName(), PATHINFO_FILENAME);
        $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);
    }
}
