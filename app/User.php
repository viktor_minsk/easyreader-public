<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','why','invite_reason'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts=[
        'reader_options'=>'array'
    ];
    static  public function findOrCreateSocial($type, $social)
    {
        $socialId=$type.'_'.$social->getId();
        $user=User::where('social_id', $socialId)->first();

        if($user) return $user;

        $user=new User();

        $user->social_id=$socialId;
        $user->save();
        return $user;

    }
}
